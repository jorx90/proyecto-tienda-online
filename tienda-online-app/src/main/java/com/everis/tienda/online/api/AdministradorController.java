package com.everis.tienda.online.api;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.tienda.online.api.base.BaseRestController;
import com.everis.tienda.online.dto.AdministradorDto;
import com.everis.tienda.online.entity.Administrador;
import com.everis.tienda.online.repository.AdministradorRepository;

@RequestMapping("/administrador")
@RestController("administradorController")
public class AdministradorController extends BaseRestController<AdministradorRepository, AdministradorDto, Administrador, Number>{

	public AdministradorController(final AdministradorRepository repository) {
		super(repository, AdministradorDto.class, Administrador.class);
	}
	
	@PostMapping("/saveEncryptedPassword")
	public ResponseEntity<AdministradorDto> saveEncryptedPassword(@RequestBody AdministradorDto admin) {
		admin.setContrasenya(DigestUtils.sha1Hex(admin.getContrasenya()));
		return this.save(admin);
	}
}
