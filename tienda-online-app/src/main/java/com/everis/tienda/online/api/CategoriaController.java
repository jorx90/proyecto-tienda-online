package com.everis.tienda.online.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.tienda.online.api.base.BaseRestController;
import com.everis.tienda.online.dto.CategoriaDto;
import com.everis.tienda.online.entity.Categoria;
import com.everis.tienda.online.repository.CategoriaRepository;

@RequestMapping("/categoria")
@RestController("categoriaController")
public class CategoriaController extends BaseRestController<CategoriaRepository, CategoriaDto, Categoria, Number>{

	public CategoriaController(final CategoriaRepository repository) {
		super(repository, CategoriaDto.class, Categoria.class);
	}
	
	
}
