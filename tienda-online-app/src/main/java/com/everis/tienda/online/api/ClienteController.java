package com.everis.tienda.online.api;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.tienda.online.api.base.BaseRestController;
import com.everis.tienda.online.dto.ClienteDto;
import com.everis.tienda.online.entity.Cliente;
import com.everis.tienda.online.repository.ClienteRepository;

@RequestMapping("/cliente")
@RestController("clienteController")
public class ClienteController extends BaseRestController<ClienteRepository, ClienteDto, Cliente, Number> {
	
	public ClienteController(final ClienteRepository repository) {
		super(repository, ClienteDto.class, Cliente.class);
	}
	
	@PostMapping("/saveEncryptedPassword")
	public ResponseEntity<ClienteDto> saveEncryptedPassword(@RequestBody ClienteDto cliente) {
		cliente.setContrasenya(DigestUtils.sha1Hex(cliente.getContrasenya()));
		return this.save(cliente);
	}
}
