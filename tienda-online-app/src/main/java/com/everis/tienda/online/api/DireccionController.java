package com.everis.tienda.online.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.tienda.online.api.base.BaseRestController;
import com.everis.tienda.online.dto.DireccionDto;
import com.everis.tienda.online.entity.Direccion;
import com.everis.tienda.online.repository.DireccionRepository;

@RequestMapping("/direccion")
@RestController("direccionController")
public class DireccionController extends BaseRestController<DireccionRepository, DireccionDto, Direccion, Number>{

	public DireccionController(final DireccionRepository repository) {
		super(repository, DireccionDto.class, Direccion.class);
	}
	
}