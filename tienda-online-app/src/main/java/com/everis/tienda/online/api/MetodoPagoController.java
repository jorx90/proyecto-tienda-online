package com.everis.tienda.online.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.tienda.online.api.base.BaseRestController;
import com.everis.tienda.online.dto.MetodoPagoDto;
import com.everis.tienda.online.entity.MetodoPago;
import com.everis.tienda.online.repository.MetodoPagoRepository;

@RequestMapping("/metodoPago")
@RestController("metodoPagoController")
public class MetodoPagoController extends BaseRestController<MetodoPagoRepository, MetodoPagoDto, MetodoPago, Number>{

	public MetodoPagoController(final MetodoPagoRepository repository) {
		super(repository, MetodoPagoDto.class, MetodoPago.class);
	}
	
	
}
