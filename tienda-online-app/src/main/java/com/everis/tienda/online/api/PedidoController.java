package com.everis.tienda.online.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.tienda.online.api.base.BaseRestController;
import com.everis.tienda.online.dto.PedidoDto;
import com.everis.tienda.online.entity.Pedido;
import com.everis.tienda.online.repository.PedidoRepository;

@RequestMapping("/pedido")
@RestController("pedidoController")
public class PedidoController extends BaseRestController<PedidoRepository, PedidoDto, Pedido, Number>{

	public PedidoController(final PedidoRepository repository) {
		super(repository, PedidoDto.class, Pedido.class);
	}
	
	
}
