package com.everis.tienda.online.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.tienda.online.api.base.BaseRestController;
import com.everis.tienda.online.dto.ProductoDto;
import com.everis.tienda.online.entity.Producto;
import com.everis.tienda.online.repository.ProductoRepository;

@RequestMapping("/producto")
@RestController("productoController")
public class ProductoController extends BaseRestController<ProductoRepository, ProductoDto, Producto, Number>{

	public ProductoController(final ProductoRepository repository) {
		super(repository, ProductoDto.class, Producto.class);
	}
	
}
