package com.everis.tienda.online.api;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.tienda.online.api.base.BaseRestController;
import com.everis.tienda.online.dto.UsuarioDto;
import com.everis.tienda.online.entity.Usuario;
import com.everis.tienda.online.repository.UsuarioRepository;

@RequestMapping("/usuario")
@RestController("usuarioController")
public class UsuarioController<R extends JpaRepository<E, ID>, E extends Usuario, O extends UsuarioDto, ID extends Number> extends BaseRestController<UsuarioRepository, UsuarioDto, Usuario, Number> {

	public UsuarioController(final UsuarioRepository repository) {
		super(repository, UsuarioDto.class, Usuario.class);
	}
	
}
