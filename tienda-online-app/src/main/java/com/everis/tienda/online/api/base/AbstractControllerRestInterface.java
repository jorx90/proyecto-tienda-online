package com.everis.tienda.online.api.base;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.everis.tienda.online.dto.ControllerObjectDTO;
import com.everis.tienda.online.util.Response;

public interface AbstractControllerRestInterface<O extends ControllerObjectDTO, ID extends Number> {

	public ResponseEntity<Long> count();
	
	public ResponseEntity<Long> countByExample(O object);
	
	public ResponseEntity<Response> delete(O object);
	
	public ResponseEntity<Response> deleteById(ID id);
	
	public ResponseEntity<Response> deleteByListExample(List<O> objects);
	
	public ResponseEntity<Response> deleteAll();
	
	public ResponseEntity<Boolean> existsByExample(O object);
	
	public ResponseEntity<Boolean> existsById(ID id);
	
	public ResponseEntity<List<O>> findAll();
	
	public ResponseEntity<List<O>> findAllByExample(O object);
	
	public ResponseEntity<List<O>> findAllByListId(ID[] ids);
	
	public ResponseEntity<O> findOneByExample(O object);
	
	public ResponseEntity<O> findById(ID id);

	public ResponseEntity<O> save(O object);

	public ResponseEntity<List<O>> saveListExample(List<O> objects);
	
}
