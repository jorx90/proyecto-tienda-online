package com.everis.tienda.online.api.base;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

import com.everis.tienda.online.dto.ControllerObjectDTO;
import com.everis.tienda.online.dto.ControllerObjectDTOImp;
import com.everis.tienda.online.entity.BaseEntity;
import com.everis.tienda.online.util.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class BaseRestController<R extends JpaRepository<E, ID>, O extends ControllerObjectDTO, E, ID extends Number>
		implements AbstractControllerRestInterface<O, ID> {

	protected R repository;

	private Class<O> dtoClass;

	private Class<E> entityClass;

	private ModelMapper modelMapper;
	
	protected final Log logger = LogFactory.getLog(getClass());

	public BaseRestController(R repository, Class<O> dtoClass, Class<E> entityClass) {
		super();
		this.repository = repository;
		this.dtoClass = dtoClass;
		this.entityClass = entityClass;
		this.modelMapper = new ModelMapper();
	}

	@Override
	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		logger.debug("count - Contando los objetos de la base de datos");
		return new ResponseEntity<>(repository.count(), HttpStatus.OK);
	}

	@Override
	@PostMapping("/countByExample")
	public ResponseEntity<Long> countByExample(@RequestBody O object) {
		logger.debug("countByExample - Contando los objetos de la base de datos mediante el objeto dado");
		return new ResponseEntity<>(repository.count(Example.of(convertToEntity(object))), HttpStatus.OK);
	}

	@Override
	@DeleteMapping("/delete")
	public ResponseEntity<Response> delete(@RequestBody O object) {
		logger.debug("delete - Eliminando objetos de la base de datos mediante el objeto dado");
		repository.delete(convertToEntity(object));
		BodyBuilder builder = ResponseEntity.ok();
		builder.allow(HttpMethod.DELETE);
		builder.contentType(MediaType.APPLICATION_JSON);
		return builder.body(new Response("Elemento eliminado correctamente"));
	}

	@Override
	@DeleteMapping("/deleteById")
	public ResponseEntity<Response> deleteById(@RequestParam final ID id) {
		logger.debug("deleteById - Eliminando objetos de la base de datos mediante el id dado");
		try {
			repository.deleteById(id);
		} catch (IllegalArgumentException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, 
					"El parámetro introducido no es válido", e);
		}
		BodyBuilder builder = ResponseEntity.ok();
		builder.allow(HttpMethod.DELETE);
		builder.contentType(MediaType.APPLICATION_JSON);
		return builder.body(new Response("Elemento eliminado correctamente"));
	}

	@Override
	@DeleteMapping("/deleteByListExample")
	public ResponseEntity<Response> deleteByListExample(@RequestBody List<O> object) {
		logger.debug("deleteByListExample - Eliminando objetos de la base de datos mediante la lista dada");
		repository.deleteAll(convertToEntity(object));
		BodyBuilder builder = ResponseEntity.ok();
		builder.allow(HttpMethod.DELETE);
		builder.contentType(MediaType.APPLICATION_JSON);
		return builder.body(new Response("Elementos eliminados correctamente"));
	}

	@Override
	@DeleteMapping("/deleteAll")
	public ResponseEntity<Response> deleteAll() {
		logger.debug("deleteAll - Eliminando todos los objetos de la base de datos");
		repository.deleteAll();
		BodyBuilder builder = ResponseEntity.ok();
		builder.allow(HttpMethod.DELETE);
		builder.contentType(MediaType.APPLICATION_JSON);
		return builder.body(new Response("Elementos eliminados correctamente"));
	}
	
	@Override
	@GetMapping("/findById")
	public ResponseEntity<O> findById(@RequestParam ID id) {
		logger.debug("findById - Buscar un objeto por Id");
		return new ResponseEntity<>(convertToDto(repository.findById(id).get()), HttpStatus.OK);
	}

	@Override
	@GetMapping("/findAll")
	public ResponseEntity<List<O>> findAll() {
		logger.debug("findAll - Buscar todos los objetos de la tabla");
		List<O> result = (List<O>) convertToDto(repository.findAll());
		return new ResponseEntity<>(result, !result.isEmpty() ? HttpStatus.OK : HttpStatus.NO_CONTENT);
	}

	@Override
	@PostMapping("/save")
	public ResponseEntity<O> save(@RequestBody O object) {
		logger.debug("save - Guardando objeto en la tabla");
		E e = convertToEntity(object);
		e = repository.save(e);
		O result = (O) convertToDto(e);
		return new ResponseEntity<>(result, result != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@Override
	@PostMapping("/existsByExample")
	public ResponseEntity<Boolean> existsByExample(@RequestBody O object) {
		logger.debug("existsByExample - Comprobando si existe objeto en la tabla");
		if (convertToEntity(object) == null) {
			new ResponseEntity<>(Boolean.FALSE, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(repository.exists(Example.of(convertToEntity(object))), HttpStatus.OK);
	}

	@Override
	@GetMapping("/existsById")
	public ResponseEntity<Boolean> existsById(@RequestParam ID id) {
		logger.debug("existsById - Comprobando si existe id en la tabla");
		return new ResponseEntity<>(repository.existsById(id), HttpStatus.OK);
	}

	@Override
	@PostMapping("/findAllByExample")
	public ResponseEntity<List<O>> findAllByExample(@RequestBody O object) {
		logger.debug("findAllByExample - Buscando en la tabla por objeto dado");
		List<O> result = (List<O>) convertToDto(repository.findAll(Example.of(convertToEntity(object))));
		return new ResponseEntity<>(result, 
				(result != null && !result.isEmpty()) ? HttpStatus.OK : HttpStatus.NO_CONTENT);
	}

	@Override
	@GetMapping("/findAllByListId")
	public ResponseEntity<List<O>> findAllByListId(@RequestParam ID[] ids) {
		logger.debug("findAllByListId - Buscando objetos en la tabla por lista de ids");
		List<O> result = (List<O>) convertToDto(repository.findAllById(Arrays.asList(ids)));
		return new ResponseEntity<>(result, !result.isEmpty() ? HttpStatus.OK : HttpStatus.NO_CONTENT);
	}

	@Override
	@PostMapping("/findOneByExample")
	public ResponseEntity<O> findOneByExample(@RequestBody O object) {
		logger.debug("findOneByExample - Buscando un objeto en la tabla");
		O result = (O) convertToDto((E) repository.findOne(Example.of(convertToEntity(object))));
		return new ResponseEntity<>(result, result != null ? HttpStatus.OK : HttpStatus.NO_CONTENT);
	}

	@Override
	@PostMapping("/saveListExample")
	public ResponseEntity<List<O>> saveListExample(@RequestBody List<O> objects) {
		logger.debug("saveListExample - Guardando lista de objetos en la tabla");
		List<O> result = (List<O>) convertToDto(repository.saveAll(convertToEntity(objects)));
		return new ResponseEntity<>(result, !result.isEmpty() ? HttpStatus.OK : HttpStatus.NO_CONTENT);
	}
	
	
	
	/*
	 * Métodos para la conversión de Dto a Entity y viceversa
	 */

	protected List<ControllerObjectDTOImp> convertToDtoBastard(Page<?> page) {
		return page.getContent().stream()
				.map(entity -> convertToDtoBastard(entity, getClassToConvert(entity.getClass().getSimpleName())))
				.collect(Collectors.toList());

	}

	protected Class getClassToConvert(String fromClass) {
		Class toClass = null;
		try {
			if (fromClass.endsWith("Dto")) {
				toClass = Class.forName("es.ico.prototipo.domain.".concat(fromClass.replaceAll("Dto", "")));
			} else {
				toClass = Class.forName("es.ico.prototipo.dto.".concat(fromClass).concat("Dto"));
			}
		} catch (Exception e) {

		}
		return toClass;

	}

	/**
	 * Convert to repository t.
	 *
	 * @param entity the entity
	 * @return the t
	 */
	protected O convertToDto(final E entity) {
		return entity != null ? this.convertFromInstanceToClass(entity, dtoClass) : null;
	}

	/**
	 * Convert to entity e.
	 *
	 * @param dto the repository
	 * @return the e
	 */
	protected E convertToEntity(final O dto) {
		return this.convertFromInstanceToClass(dto, entityClass);
	}

	/**
	 * Convert to repository t.
	 *
	 * @param entity the entity
	 * @return the t
	 */
	protected ControllerObjectDTOImp convertToDtoBastard(final Object entity, final Class toClass) {
		return (ControllerObjectDTOImp) this.convertFromInstanceToClassBastard(entity, toClass);
	}

	/**
	 * Convert to repository t.
	 *
	 * @param entity the entity
	 * @return the t
	 */
	protected ControllerObjectDTOImp convertToDtoBastard(final Object entity) {
		return (ControllerObjectDTOImp) this.convertToDtoBastard(entity,
				getClassToConvert(entity.getClass().getSimpleName()));
	}

	/**
	 * Convert to entity e.
	 *
	 * @param dto the repository
	 * @return the e
	 */
	protected Object convertToEntityBastard(final Object dto, final Class toClass) {
		return this.convertFromInstanceToClassBastard(dto, toClass);
	}

	/**
	 * Convert from instance to class object.
	 *
	 * @param                    <C> the type parameter
	 * @param fromObjectInstance the from object instance
	 * @param toClass            the to class
	 * @return the object
	 */
	protected <C extends Object> C convertFromInstanceToClass(final Object fromObjectInstance, final Class<C> toClass) {
		return modelMapper.map(fromObjectInstance, toClass);
	}

	/**
	 * Convert from instance to class object.
	 *
	 * @param fromObjectInstance the from object instance
	 * @param toClass            the to class
	 * @return the object
	 */
	protected Object convertFromInstanceToClassBastard(final Object fromObjectInstance, final Class toClass) {
		try {
			new ObjectMapper().writeValueAsString(fromObjectInstance);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return modelMapper.map(fromObjectInstance, toClass);
	}

	/**
	 * Convert to repository collection.
	 *
	 * @param entityCollection the entity list
	 * @return the collection
	 */
	protected Collection<O> convertToDto(final Collection<E> entityCollection) {
		return entityCollection.stream().map(entity -> convertToDto(entity)).collect(Collectors.toList());
	}

	/**
	 * Convert to repository collection.
	 *
	 * @param entityCollection the entity list
	 * @return the collection
	 */
	protected Collection<O> convertToDto(final Iterable<E> entityCollection) {
		return ((Collection<E>) entityCollection).stream().map(entity -> convertToDto(entity))
				.collect(Collectors.toList());
	}

	/**
	 * Convert to entity collection.
	 *
	 * @param dtoList the repository list
	 * @return the collection
	 */
	protected Collection<E> convertToEntity(final Collection<O> dtoList) {
		return dtoList.stream().map(dto -> convertToEntity(dto)).collect(Collectors.toList());
	}

	/**
	 * Convert to repository collection.
	 *
	 * @param entityCollection the entity list
	 * @param toClass          the to class
	 * @return the collection
	 */
	protected List<ControllerObjectDTOImp> convertToDtoBastard(Collection<BaseEntity> entityCollection, Class toClass) {
		return toClass != null
				? entityCollection.stream().map(entity -> convertToDtoBastard(entity, toClass))
						.collect(Collectors.toList())
				: entityCollection.stream().map(
						entity -> convertToDtoBastard(entity, getClassToConvert(entity.getClass().getSimpleName())))
						.collect(Collectors.toList());
	}

	/**
	 * Convert to entity collection.
	 *
	 * @param dtoList the repository list
	 * @param toClass the to class
	 * @return the collection
	 */
	protected Collection<Serializable> convertToEntityBastard(final Collection<ControllerObjectDTO> dtoList,
			final Class toClass) {
		return toClass != null
				? dtoList.stream().map(dto -> (Serializable) convertToEntityBastard(dto, toClass))
						.collect(Collectors.toList())
				: dtoList.stream().map(dto -> (Serializable) convertToEntityBastard(dto,
						getClassToConvert(dto.getClass().getSimpleName()))).collect(Collectors.toList());
	}

}
