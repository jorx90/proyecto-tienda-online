package com.everis.tienda.online.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.everis.tienda.online.util.Response;

@ControllerAdvice
public class DefaultHandlerException {

	@ExceptionHandler({ IllegalArgumentException.class, NullPointerException.class, HttpMessageNotReadableException.class })
	public ResponseEntity<Response> handleException(Exception exception) {
		HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		String message = "Error de conexión con el servidor";
		if (exception instanceof NullPointerException) {
			message = "Error de puntero nulo";
			httpStatus = HttpStatus.BAD_REQUEST;
		} else if (exception instanceof IllegalArgumentException) {
			message = "Error, el número de parámetros recibido no es correcto";
			httpStatus = HttpStatus.BAD_REQUEST;
		} else if (exception instanceof HttpMessageNotReadableException) {
			message = "Error en el formato de JSON recibido";
			httpStatus = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(new Response(message, httpStatus.value()), httpStatus);
	}
}
