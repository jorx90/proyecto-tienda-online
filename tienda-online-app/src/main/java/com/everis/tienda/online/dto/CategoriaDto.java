package com.everis.tienda.online.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CategoriaDto extends ControllerObjectDTOImp {
	
	private BigDecimal id;
	
	private String codigo;
	
	private String descripcion;
	
}
