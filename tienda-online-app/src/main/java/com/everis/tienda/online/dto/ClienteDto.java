package com.everis.tienda.online.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ClienteDto extends UsuarioDto {
	
	private String email;
	
	private List<DireccionDto> direccion;

}