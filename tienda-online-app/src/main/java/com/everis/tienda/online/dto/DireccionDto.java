package com.everis.tienda.online.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DireccionDto extends ControllerObjectDTOImp {

	private BigDecimal id;
	
	private String direccion;
	
	private String numero;
	
	private String piso;
	
	private String puerta;
	
	private String codigoPostal;
	
	private String poblacion;
	
	private String provincia;
	
	private String pais;
	
}
