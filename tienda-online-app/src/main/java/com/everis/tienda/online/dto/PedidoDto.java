package com.everis.tienda.online.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PedidoDto extends ControllerObjectDTOImp {
	
	private BigDecimal id;
	
	private Date fecha;
	
	private ClienteDto cliente;
	
	private BigDecimal importe;
	
	private MetodoPagoDto metodoPago;
	
	private Set<ProductoDto> producto = new HashSet<>();

}
