package com.everis.tienda.online.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProductoDto extends ControllerObjectDTOImp {
	
	private BigDecimal id;
	
	private String ean13;
	
	private String descripcion;
	
	private String imagen;
	
	private BigDecimal precio;
	
	private CategoriaDto categoria;
	
}
