package com.everis.tienda.online.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Response {
	
	private String message;
	
	private Integer status;

	public Response(String message) {
		super();
		this.message = message;
	}
	
}
