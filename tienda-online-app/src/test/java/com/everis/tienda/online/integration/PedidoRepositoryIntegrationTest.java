package com.everis.tienda.online.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.everis.tienda.online.entity.Categoria;
import com.everis.tienda.online.entity.Cliente;
import com.everis.tienda.online.entity.MetodoPago;
import com.everis.tienda.online.entity.Pedido;
import com.everis.tienda.online.entity.Producto;
import com.everis.tienda.online.repository.PedidoRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PedidoRepositoryIntegrationTest {

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private PedidoRepository pedidoRepository;
	
	private Pedido pedido;
	private Cliente cliente;
	private MetodoPago metodoPago;
	private Producto producto;	
	private Set<Producto> productos = new HashSet<>();
	
	@Before
	public void crearPedido() {
		pedido = new Pedido();
		cliente = new Cliente();
		metodoPago = new MetodoPago();
		producto = new Producto();
		
		producto.setDescripcion("descProducto");
		producto.setEan13("8342883242349");
		producto.setImagen("/ruta");
		producto.setPrecio(BigDecimal.valueOf(250));
		productos.add(producto);
		
		metodoPago.setDescripcion("Tarjeta");
		
		cliente.setNombre("Nombre cliente");
		cliente.setUsuario("user");
		cliente.setContrasenya("pass");
		cliente.setEmail("email@email.com");
		
		pedido = new Pedido();
		pedido.setFecha(new Date());
		pedido.setImporte(BigDecimal.TEN);
	}
	
	@Test
	public void whenFindAll_thenReturnAdministadorList() {
		List<Pedido> list = new ArrayList<>();
		Pedido ped = new Pedido();
		ped.setFecha(new Date());
		ped.setImporte(BigDecimal.TEN);
		list.add(ped);
		
		for (Pedido pedido : list) {
			entityManager.persist(pedido);
		}
		entityManager.flush();
		
		List<Pedido> result = pedidoRepository.findAll();
		
		assertThat(result.get(0).getImporte()).isEqualTo(list.get(0).getImporte());
	}
	
	@Test
	public void savePedido() {
		entityManager.persist(cliente);
		entityManager.persist(producto);
		entityManager.persist(metodoPago);
		
		pedido.setProducto(productos);
		pedido.setMetodoPago(metodoPago);
		pedido.setCliente(cliente);
		
		entityManager.persist(pedido);
		
		assertNotNull(pedidoRepository.save(pedido));
	}
}
