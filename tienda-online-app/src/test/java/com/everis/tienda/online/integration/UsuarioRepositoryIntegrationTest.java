package com.everis.tienda.online.integration;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.everis.tienda.online.entity.Usuario;
import com.everis.tienda.online.repository.UsuarioRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuarioRepositoryIntegrationTest {

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Test
	public void whenFindAll_thenReturnAdministadorList() {
		List<Usuario> list = new ArrayList<>();
		Usuario user = new Usuario();
		user.setNombre("Paco");
		user.setApellidos("Martinez");
		user.setUsuario("pmart");
		user.setContrasenya("123456");
		list.add(user);
		
		for (Usuario usuario : list) {
			entityManager.persist(usuario);
		}
		entityManager.flush();
		
		List<Usuario> result = usuarioRepository.findAll();
		
		assertThat(result.get(0).getNombre()).isEqualTo(list.get(0).getNombre());
	}
}
