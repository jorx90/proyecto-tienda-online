package com.everis.tienda.online.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="ADMINISTRADOR")
@Inheritance(strategy = InheritanceType.JOINED)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Administrador extends Usuario {

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 5952257624492860106L;
	
	@Column(name = "CLAVE")
	private String clave;

}
