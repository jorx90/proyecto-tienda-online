package com.everis.tienda.online.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="CLIENTE")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Cliente extends Usuario {
	
	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 9063605347154613485L;

	@Column(name="EMAIL")
	private String email;
	
	@OneToMany(mappedBy = "cliente")
	private Set<Pedido> pedido;
	
	@OneToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	@JoinColumn(name = "CLIENTE_ID")
	private Set<Direccion> direccion;
	
}
