package com.everis.tienda.online.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="DIRECCION")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Direccion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigDecimal id;
	
	@Column(name="DIRECCION")
	private String direccion;
	
	@Column(name="NUMERO")
	private String numero;
	
	@Column(name="PISO")
	private String piso;
	
	@Column(name="PUERTA")
	private String puerta;
	
	@Column(name="CODIGO_POSTAL")
	private String codigoPostal;
	
	@Column(name="POBLACION")
	private String poblacion;
	
	@Column(name="PROVINCIA")
	private String provincia;
	
	@Column(name="PAIS")
	private String pais;
	
	@ManyToOne(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	private Cliente cliente;
}
