package com.everis.tienda.online.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="PRODUCTO")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Producto implements Serializable {
	
	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 3850880467909498016L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigDecimal id;
	
	@Column(name="EAN13")
	private String ean13;
	
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	@Column(name="IMAGEN")
	private String imagen;
	
	@Column(name="PRECIO")
	private BigDecimal precio;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CATEGORIA")
	private Categoria categoria;
	
	@ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "PEDIDO_PRODUCTO", 
        joinColumns = { @JoinColumn(name = "PRODUCTO_ID") }, 
        inverseJoinColumns = { @JoinColumn(name = "PEDIDO_ID") }
    )
	private Set<Pedido> pedido = new HashSet<>();
	
}
