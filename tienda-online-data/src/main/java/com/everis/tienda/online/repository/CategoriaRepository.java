package com.everis.tienda.online.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.everis.tienda.online.entity.Categoria;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Number> {
	
}
