package com.everis.tienda.online.repository;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.everis.tienda.online.entity.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Number> {
	
	@Query("SELECT c FROM Cliente c JOIN FETCH c.direccion WHERE c.id = ?1")
	Cliente findById(BigDecimal id);
	
}
