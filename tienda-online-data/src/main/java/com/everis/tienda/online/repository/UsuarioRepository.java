package com.everis.tienda.online.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.everis.tienda.online.entity.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Number> {
	
}
